package com.code.ivan.topmovies.models

data class MoviesList(
        val results: List<Movie>)