package com.code.ivan.topmovies

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.code.ivan.topmovies.api.ApiInterface
import com.code.ivan.topmovies.models.Movie
import io.reactivex.disposables.CompositeDisposable


class MovieDataSourceFactory(private val compositeDisposable: CompositeDisposable,
                             private val movieService: ApiInterface) : DataSource.Factory<String, Movie>() {

    val dataSourceLiveData = MutableLiveData<MovieDataSource>()
    var query = ""
    override fun create(): DataSource<String, Movie> {
        val movieDataSource = MovieDataSource(movieService, compositeDisposable, query)
        dataSourceLiveData.postValue(movieDataSource)
        return movieDataSource
    }

}