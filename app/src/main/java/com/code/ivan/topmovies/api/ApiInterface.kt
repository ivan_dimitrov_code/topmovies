package com.code.ivan.topmovies.api

import com.code.ivan.topmovies.models.MoviesList
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    @GET("4/list/{page}/")
    fun getMovies(@Path("page") page: Int,
                  @Query("api_key") key: String): Flowable<MoviesList>

    @GET("3/search/movie/")
    fun getMoviesForQuery(@Query("page") page: Int,
                          @Query("query") query: String,
                          @Query("api_key") key: String): Flowable<MoviesList>
}