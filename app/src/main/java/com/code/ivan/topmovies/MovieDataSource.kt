package com.code.ivan.topmovies

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.ItemKeyedDataSource
import com.code.ivan.topmovies.api.ApiClient
import com.code.ivan.topmovies.api.ApiInterface
import com.code.ivan.topmovies.models.Movie
import io.reactivex.disposables.CompositeDisposable

class MovieDataSource(private val moveApi: ApiInterface,
                      private val compositeDisposable: CompositeDisposable,
                      private val query: String)
    : ItemKeyedDataSource<String, Movie>() {

    private var pageNumber = 1
    val loadState = MutableLiveData<DataLoadState>()

    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<Movie>) {
        loadState.postValue(DataLoadState.LOADING)
        requestMovieList(callback)
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<Movie>) {
        loadState.postValue(DataLoadState.LOADING)
        requestMovieList(callback)
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<Movie>) {
    }

    override fun getKey(item: Movie): String = item.title

    private fun requestMovieList(callback: LoadCallback<Movie>) {
        loadState.postValue(DataLoadState.LOADING)
        if (query == "") {
            requestTopMovies(callback)
        } else {
            requestMovieForQuery(callback)
        }
    }

    private fun requestTopMovies(callback: LoadCallback<Movie>) {
        compositeDisposable.add(moveApi.getMovies(pageNumber++, ApiClient.API_KEY).subscribe { movies ->
            loadState.postValue(DataLoadState.LOADED)
            callback.onResult(movies.results)
        })
    }

    private fun requestMovieForQuery(callback: LoadCallback<Movie>) {
        compositeDisposable.add(
                moveApi.getMoviesForQuery(pageNumber++, query, ApiClient.API_KEY).subscribe({ movies ->
                    loadState.postValue(DataLoadState.LOADED)
                    callback.onResult(movies.results)
                }, {
                    loadState.postValue(DataLoadState.FAILED)
                }))
    }

}