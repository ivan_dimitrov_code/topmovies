package com.code.ivan.topmovies.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class ApiClient {
    companion object {
        private const val baseUrl = "https://api.themoviedb.org/"
        const val API_KEY = "93aea0c77bc168d8bbce3918cefefa45"
        private var retrofit: ApiInterface? = null

        fun getClient(): ApiInterface? {
            if (retrofit == null) {

                retrofit = Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addConverterFactory(GsonConverterFactory.create())
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .build()
                        .create(ApiInterface::class.java)
            }
            return retrofit
        }
    }
}