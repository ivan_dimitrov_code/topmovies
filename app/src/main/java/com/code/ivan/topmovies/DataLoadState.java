package com.code.ivan.topmovies;

public enum DataLoadState {
        LOADING,
        LOADED,
        FAILED
}
