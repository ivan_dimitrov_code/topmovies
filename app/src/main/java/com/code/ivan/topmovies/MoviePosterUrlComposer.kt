package com.code.ivan.topmovies

fun composeUrlForMovie(posterPath: String): String {
    return "https://image.tmdb.org/t/p/w500/$posterPath"
}