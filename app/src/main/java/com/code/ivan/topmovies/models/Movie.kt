package com.code.ivan.topmovies.models

import com.google.gson.annotations.SerializedName

data class Movie(
        var title: String,
        @SerializedName("release_date") var releaseDate: String,
        var overview: String,
        @SerializedName("poster_path") var posterId: String)